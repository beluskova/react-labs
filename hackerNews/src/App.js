 import React from 'react';

    var NewsForm = React.createClass({
          render : function() {
            return (
               <form style={{marginTop: '30px'}}>
                  <h3>Add a new post</h3>

                  <div className="form-group">
                    <input type="text"
                      className="form-control"
                      placeholder="Title"></input>
                  </div>
                  <div className="form-group">
                    <input type="text"
                    className="form-control"
                    placeholder="Link"></input>
                  </div>
                  <button type="submit" className="btn btn-primary">Post</button>
                </form>
                );
            }
     });

    var NewsItem = React.createClass({
          render : function() {
              var divStyle = {
                   fontSize: '20px', 
                   marginLeft: '10px' 
                  };
              var cursor = { cursor: 'pointer' } ;
              var line ;
              if (this.props.post.link !== null ) {
                 line = <a href={this.props.post.link} >
                {            this.props.post.title} </a> ;
              } else {
                 line = <span>{this.props.post.title} </span> ;
              }
            return (
                <div>
                  <span className="glyphicon glyphicon-thumbs-up"
                        style={cursor} />
                  {this.props.post.upvotes}
                  <span style={divStyle}>{line}
                      <span>
                      <a href={'#/posts/' + this.props.post.id }>Comments</a>
                    </span>
                  </span>
                </div>  
                );
          }
     }) ;

    // TODO (missing component)

      var NewsList = React.createClass({
          shouldComponentUpdate : function(nextProps,nextState) {
              if (this.props.posts.length === nextProps.posts.length ) {
                return false ;
              } else {
                return true ;
              }     
          },
          render : function() {
              var items = this.props.posts.map(item => {
                     return <NewsItem key={item.title} post={item} />
                 });
              return (
                <ul>
                   {items}
                 </ul>
                )
          }
      });   

     var HackerApp = React.createClass({
        render: function(){
            return (
                <div className="container">
                  <div className="row">
                    <div className="col-md-6 col-md-offset-3">
                 <div className="page-header">
                         {/* TODO */}
                          <h1>Hacker News</h1>
                       <NewsList posts={this.props.posts}  />
                        <NewsForm />
                 </div>
                   </div>
                  </div>
                </div>
            );
        }
    });

    export default HackerApp;